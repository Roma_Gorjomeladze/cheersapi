const mongoose = require('mongoose')
const router = require('express').Router()
const UserModel = require('../models/userModel.js')
const PostModel = require('../models/postModel.js')



exports.registration = function(req, res) {
	let newUser = new UserModel({
		firstname: req.body.firstname,
		lastname: req.body.lastname,
		password: req.body.password,
		email: req.body.email,
		showradio: req.body.showradio,
		birthdate: req.body.birthdate,
		
	})
	UserModel.find({email: req.body.email}).then((user)=>{
		if(user.length === 0 || !user){
			newUser.save().then(()=>{
				res.send({message: "registration success"})
			})
		}else{
			res.send({message: "such user already exists"})
		}
	})
}

exports.userLogin = function(req,res){
	let user = {
		email: req.body.email,
		password: req.body.password
	}
	UserModel.find({email: req.body.email}).then((user)=>{
		if(!user || user.length === 0){
			console.log('no such user')
			res.status(200).send({
				message: "there is not such email, please register first or try again later"
			})
		}else if(user[0].password !== req.body.password){
			res.status(200).send({
				message: " your password is incorrect, please try again"
			})
		}else{
			console.log('you are loged in')
			res.status(200).send({
				message: "you are loged in, redirect to profile"
			})
		}
	})
}

exports.usersList = function(req, res){
	UserModel.find().then((users)=>{
		res.send({users: users})
	})
}

exports.addPost = function(req, res){
	let newPost = new PostModel({
		title: req.body.title,
		bodyText: req.body.bodyText,
		likes: req.body.likes,
		comments: req.body.comments,
		authorId: req.body.authorId,
		date: req.body.date
	})
	newPost.save().then(()=>{
		res.send({
			message: "your post has been created"
		})
	})
}

exports.getPosts = function(req, res){
	PostModel.find().then(posts =>{
		res.json(posts)
	})
}


exports.test = function(req, res){
	res.send({
		message: 'success'
	})
}