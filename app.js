const express = require('express');
const mongoose = require('mongoose');
const router = require('./routs/userRouts.js')
require('dotenv').config()
// const passportSetup = require('./configs/passport.setup.js')
const URI = process.env.URI
let app = express()


app.use(express.json());
app.use('/', router)

mongoose.connect(URI, {useNewUrlParser: true}, ()=>{
	console.log('mongoose success')
});


app.listen(4000, ()=>console.log('listening on 4000'))