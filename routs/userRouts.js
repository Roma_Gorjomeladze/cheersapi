const router = require('express').Router()
const userControllers = require('../controllers/userControllers.js')

// const passport = require('passport')

router.post('/registration', userControllers.registration)

router.get('/users/list', userControllers.usersList)

router.post('/login', userControllers.userLogin)

router.post('/user/addpost', userControllers.addPost)

router.get('/user/posts', userControllers.getPosts)

router.get('/test', userControllers.test)

module.exports = router;