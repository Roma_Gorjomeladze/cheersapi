const mongoose = require('mongoose')
let Schema = mongoose.Schema

let UserSchema = new Schema({
	firstname: {type: String, require: true},
	lastname: {type: String, require: true},
	password: {type: String, require: true},
	email: {type: String, require: true},
	showradio: {type: Boolean, require: true},
	birthdate: {type: Date, require: true}
})

const User = mongoose.model('User', UserSchema);
module.exports = User