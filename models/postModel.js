const mongoose = require('mongoose')
let Schema = mongoose.Schema

let PostSchema = new Schema({
	title: {type: String, require: true},
	bodyText: {type: String, require: true},
	likes: {type: Number, require: true},
	comments: {type: Array, require: true},
	authorId: {type: String, require: true},
	date: {type: Date, require: true}
})

const Post = mongoose.model('Post', PostSchema);
module.exports = Post